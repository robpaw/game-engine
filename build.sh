#!/bin/bash

g++-5 -std=c++14 -Wall -o array_test array_test.cpp -lgtest -lpthread
g++-5 -std=c++14 -Wall -o array_bench array_bench.cpp -lbenchmark -lpthread -O3 -fno-exceptions -fno-rtti -fno-omit-frame-pointer

g++-5 -std=c++14 -Wall -o matrix_test matrix_test.cpp -lgtest -lpthread
g++-5 -std=c++14 -Wall -o matrix_bench matrix_bench.cpp -lbenchmark -lpthread -O3 -fno-exceptions -fno-rtti -fno-omit-frame-pointer

g++-5 -std=c++14 -Wall -o quaternion_test quaternion_test.cpp -lgtest -lpthread
g++-5 -std=c++14 -Wall -o quaternion_bench quaternion_bench.cpp -lbenchmark -lpthread -O3 -fno-exceptions -fno-rtti -fno-omit-frame-pointer

g++-5 -std=c++14 main.cpp -o main -lGLEW -lglfw3 -lGL -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor -lrt -ldl -lSOIL -lm -pthread
