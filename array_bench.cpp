#include "array.hpp"

#include <benchmark/benchmark.h>

static void escape(void *p) { asm volatile("" : : "g"(p) : "memory"); }
static void clobber() { asm volatile("" : : : "memory"); }

static void bm_create(benchmark::State &state) {
  typedef yaes::util::array<int> arr_t;

  while (state.KeepRunning()) {
    arr_t a;
    escape(&a);
    (void)a;
  }
}
BENCHMARK(bm_create)->MinTime(1);

static void bm_get(benchmark::State &state) {
  typedef yaes::util::array<int> arr_t;

  arr_t arr;
  for (arr_t::size_type i = 1; i < arr.capacity(); ++i) {
    arr_t::handle_type h{i};
    arr.insert(h, i);
  }

  arr_t::value_type j;
  arr_t::size_type i = 0;
  while (state.KeepRunning()) {
    arr_t::handle_type h{i++};
    arr.get(h, j);
    escape(&j);
    if (j == arr.capacity())
      i = 0; // prevent from opt.
  }
}
BENCHMARK(bm_get)->MinTime(1);

static void bm_set(benchmark::State &state) {
  typedef yaes::util::array<int> arr_t;

  arr_t arr;
  for (arr_t::size_type i = 1; i < arr.capacity(); ++i) {
    arr_t::handle_type h{i};
    arr.insert(h, i);
  }

  arr_t::value_type j = 0;
  arr_t::size_type i = 0;
  while (state.KeepRunning()) {
    arr_t::handle_type h{i++};
    arr.set(h, j);
    escape(&j);
    clobber();
    if (i == arr.capacity())
      i = 0;
  }
}
BENCHMARK(bm_set)->MinTime(1);

static void bm_copy(benchmark::State &state) {
  typedef yaes::util::array<int> arr_t;

  arr_t arr;
  for (arr_t::size_type i = 1; i < arr.capacity(); ++i) {
    arr_t::handle_type h{i};
    arr.insert(h, i);
  }

  arr_t a;
  while (state.KeepRunning()) {
    a = arr;
    escape(&a);
    clobber();
  }
}
BENCHMARK(bm_copy)->MinTime(1);

static void bm_insert(benchmark::State &state) {
  typedef yaes::util::array<int> arr_t;

  arr_t arr;
  arr_t::size_type i = 0;
  while (state.KeepRunning()) {
    if (arr.full()) {
      state.PauseTiming();
      arr.clear();
      i = 0;
      state.ResumeTiming();
    }

    arr_t::handle_type h{i++};
    if (!arr.insert(h, i)) {
      abort();
    }
    escape(&arr);
    clobber();
  }
}
BENCHMARK(bm_insert)->MinTime(1);

static void bm_remove(benchmark::State &state) {
  typedef yaes::util::array<int> arr_t;

  arr_t arr;
  for (arr_t::size_type i = 1; i < arr.capacity(); ++i) {
    arr_t::handle_type h{i};
    arr.insert(h, i);
  }

  arr_t a;
  arr_t::size_type i = 1;
  while (state.KeepRunning()) {
    if (a.empty()) {
      state.PauseTiming();
      a = arr;
      i = 1;
      state.ResumeTiming();
    }

    arr_t::handle_type h{i++};
    if (!a.remove(h)) {
      abort();
    }
    escape(&a);
    clobber();
  }
}
BENCHMARK(bm_remove)->MinTime(1);

static void bm_sort(benchmark::State &state) {
  typedef yaes::util::array<int> arr_t;
  arr_t arr;
  arr_t::size_type min = 0, max = arr.capacity();
  for (arr_t::size_type i = 0; i < arr.capacity(); ++i) {
    arr_t::handle_type h{i};
    arr.insert(h, i % 2 == 1 ? min++ : max--);
  }
  arr_t a;
  while (state.KeepRunning()) {
    state.PauseTiming();
    a = arr;
    escape(&a);
    state.ResumeTiming();
    a.sort();
    clobber();
  }
}
BENCHMARK(bm_sort)->MinTime(1);

static void bm_sort_asc(benchmark::State &state) {
  typedef yaes::util::array<int> arr_t;
  arr_t arr;
  for (arr_t::size_type i = 0; i < arr.capacity(); ++i) {
    arr_t::handle_type h{i};
    arr.insert(h, i);
  }
  arr_t a;
  while (state.KeepRunning()) {
    state.PauseTiming();
    a = arr;
    escape(&a);
    state.ResumeTiming();
    a.sort();
    clobber();
  }
}
BENCHMARK(bm_sort_asc)->MinTime(1);

static void bm_sort_dsc(benchmark::State &state) {
  typedef yaes::util::array<int> arr_t;
  arr_t arr;
  for (arr_t::size_type i = 0; i < arr.capacity(); ++i) {
    arr_t::handle_type h{i};
    arr.insert(h, arr.capacity() - i);
  }
  arr_t a;
  while (state.KeepRunning()) {
    state.PauseTiming();
    a = arr;
    escape(&a);
    state.ResumeTiming();
    a.sort();
    clobber();
  }
}
BENCHMARK(bm_sort_dsc)->MinTime(1);

BENCHMARK_MAIN();
