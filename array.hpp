#pragma once

#include <cstddef>
#include <cstdint>
#include <cassert>
#include <cstring>
#include <algorithm>
#include <array>
#include <limits>
#include <type_traits>
#include <utility>

#include <functional>
#include <iostream>

#ifndef CACHE_LINE_SIZE
#define CACHE_LINE_SIZE 64
#endif

namespace yaes {
namespace util {

template <typename SizeType = uint16_t> struct handle {
  SizeType value;
  bool operator==(const handle &other) const { return value == other.value; }
  bool operator!=(const handle &other) const { return value != other.value; }
  template <typename Stream>
  friend Stream &operator<<(Stream &s, const handle &h) {
    s << h.value;
    return s;
  }
};

template <typename T, size_t N = 32 * 1024, typename SizeType = uint16_t>
class array {
  static_assert(N < ::std::numeric_limits<SizeType>::max(),
                "array is too large");

public:
  typedef array<T, N, SizeType> this_type;
  typedef T value_type;
  typedef T *pointer;
  typedef const T *const_pointer;
  typedef T &reference;
  typedef const T &const_reference;
  typedef SizeType size_type;
  typedef ptrdiff_t difference_type;

  typedef size_type index_type;
  typedef handle<SizeType> handle_type;

  struct entry {
    const handle_type &handle;
    value_type &value;
  };
  struct const_entry {
    const handle_type &handle;
    const value_type &value;
  };

  template <typename ArrayType, typename ValueType> class iterator_type {
  public:
    typedef ArrayType array_type;
    typedef ValueType value_type;

    typedef iterator_type<array_type, value_type> this_type;

    typedef typename array_type::index_type index_type;
    typedef typename array_type::handle_type handle_type;

    typedef std::random_access_iterator_tag iterator_category;
    typedef typename array_type::difference_type difference_type;
    typedef typename array_type::pointer pointer;
    typedef typename array_type::reference reference;

    constexpr inline iterator_type() : arr(0), pos(0) {}
    constexpr inline iterator_type(array_type *arr, index_type pos)
        : arr(arr), pos(pos) {}
    inline value_type operator*() const {
      assert(arr);
      assert(pos >= 0 && pos < arr->s);
      return value_type{arr->itoh[pos + 1], arr->storage[pos + 1]};
    }
    inline value_type operator[](difference_type n) const {
      assert(arr);
      assert(pos >= 0 && pos < arr->s);
      return value_type{arr->itoh[pos + 1], arr->storage[pos + 1]};
    }

    inline difference_type operator-(const this_type &other) const {
      assert(arr == other.arr);
      return pos - other.pos;
    }

    inline this_type &operator+=(difference_type n) {
      pos += n;
      return *this;
    }
    inline this_type operator+(difference_type n) {
      this_type it = *this;
      return it += n;
    }

    inline this_type &operator-=(difference_type n) {
      pos -= n;
      return *this;
    }
    inline this_type operator-(difference_type n) {
      this_type it = *this;
      return it -= n;
    }

    inline this_type &operator++() {
      ++pos;
      return *this;
    }
    inline this_type operator++(int) {
      this_type it = *this;
      ++pos;
      return it;
    }

    inline this_type &operator--() {
      --pos;
      return *this;
    }
    inline this_type operator--(int) {
      this_type it = *this;
      --pos;
      return it;
    }

    inline bool operator!=(const this_type &other) const {
      assert(arr == other.arr);
      return pos != other.pos;
    }
    inline bool operator==(const this_type &other) const {
      assert(arr == other.arr);
      return pos == other.pos;
    }
    inline bool operator<(const this_type &other) const {
      assert(arr == other.arr);
      return pos < other.pos;
    }
    inline bool operator<=(const this_type &other) const {
      assert(arr == other.arr);
      return pos <= other.pos;
    }
    inline bool operator>(const this_type &other) const {
      assert(arr == other.arr);
      return pos > other.pos;
    }
    inline bool operator>=(const this_type &other) const {
      assert(arr == other.arr);
      return pos >= other.pos;
    }

    inline void swap(this_type &other) {
      assert(arr == other.arr);
      std::swap(pos, other.pos);
    }
    inline void iter_swap(this_type &other) {
      assert(arr == other.arr);
      arr->swap(pos, other.pos);
    }

  private:
    array_type *arr;
    difference_type pos;
  };

  typedef iterator_type<this_type, entry> iterator;
  typedef iterator_type<const this_type, const_entry> const_iterator;

private:
  constexpr static const index_type M = N + 1;

  alignas(CACHE_LINE_SIZE) std::array<index_type, M> htoi;
  alignas(CACHE_LINE_SIZE) std::array<handle_type, M> itoh;
  alignas(CACHE_LINE_SIZE) std::array<value_type, M> storage;
  index_type s;

  inline bool is_valid(index_type i) const { return i != 0; }
  inline bool is_valid(handle_type h) const { return h.value != 0; }

public:
  inline constexpr array() { clear(); }

  inline bool get(handle_type handle, value_type &value) const {
    index_type i = htoi[handle.value];
    value = storage[i];
    return is_valid(i);
  }
  inline bool set(handle_type handle, const value_type &value) {
    index_type i = htoi[handle.value];
    storage[i] = value;
    return is_valid(i);
  }

  inline bool contains(index_type idx) { return idx < s; }
  inline bool contains(handle_type handle) {
    return is_valid(htoi[handle.value]);
  }

  inline const value_type &operator[](handle_type handle) const {
    assert(contains(handle));
    return storage[htoi[handle.value]];
  }
  inline value_type &operator[](handle_type handle) {
    assert(contains(handle));
    return storage[htoi[handle.value]];
  }

  inline const value_type &operator[](index_type idx) const {
    assert(contains(idx));
    return storage[idx + 1];
  }
  inline value_type &operator[](index_type idx) {
    assert(contains(idx));
    return storage[idx + 1];
  }

  bool insert(handle_type handle, const value_type &value) {
    index_type i = htoi[handle.value];

    if (is_valid(i) || size() == capacity()) {
      return false;
    }

    ++s;

    htoi[handle.value] = s;
    itoh[s] = handle;
    storage[s] = value;

    return true;
  }
  bool remove(handle_type handle) {
    index_type i = htoi[handle.value];

    if (!is_valid(i)) {
      return false;
    }

    htoi[itoh[s].value] = i;
    htoi[handle.value] = 0;

    std::swap(storage[i], storage[s]);
    std::swap(itoh[i], itoh[s]);

    --s;

    return true;
  }

  constexpr inline size_type max_size() const { return N; }
  constexpr inline size_type capacity() const { return N; }
  inline size_type size() const { return s; }
  void clear() {
    memset(&htoi[0], 0, sizeof(index_type) * M);
    s = 0;
  }

  bool full() const { return s == N; }
  bool empty() const { return s == 0; }

  iterator begin() { return iterator(this, 0); }
  const_iterator begin() const { return const_iterator(this, 0); }
  iterator end() { return iterator(this, s); }
  const_iterator end() const { return const_iterator(this, s); }

  inline bool operator==(const this_type &other) const {
    return s == other.s && htoi == other.htoi && itoh == other.itoh &&
           storage == other.storage;
  }
  inline bool operator!=(const this_type &other) const {
    return !operator==(other);
  }

  inline void swap(index_type idx1, index_type idx2) {
    ++idx1;
    ++idx2;
    std::swap(storage[idx1], storage[idx2]);
    std::swap(htoi[itoh[idx1].value], htoi[itoh[idx2].value]);
    std::swap(itoh[idx1], itoh[idx2]);
  }

  void sort() {
    sort(begin(), end(),
         [](const auto &v1, const auto &v2) { return v1.value < v2.value; });
  }
  void rsort() {
    sort(begin(), end(),
         [](const auto &v1, const auto &v2) { return v1.value > v2.value; });
  }
  template <typename Compare> void sort(Compare cmp) {
    sort(begin(), end(), cmp);
  }

  template <typename Compare>
  void sort(iterator first, iterator last, Compare cmp) {
    assert(first <= last);
    auto dist = std::distance(first, last);
    if (dist == 2) {
      std::advance(last, -1);
      if (!cmp(*first, *last)) {
        first.iter_swap(last);
      }
    } else if (dist > 2) {
      auto pivot = std::next(first, dist / 2);

      first.iter_swap(pivot);
      auto part = partition(std::next(first), last, [&](const auto &v) -> bool {
        return cmp(v, *first);
      });
      std::advance(part, -1);
      first.iter_swap(part);

      sort(first, part, cmp);
      std::advance(part, 1);
      sort(part, last, cmp);
    }
  }

  template <typename Predicate>
  iterator partition(iterator first, iterator last, Predicate p) {
    assert(first <= last);
    if (first == last)
      return first;
    std::advance(last, -1);
    while (first != last) {
      if (p(*first)) {
        std::advance(first, 1);
      } else {
        while (first != last && !p(*last)) {
          std::advance(last, -1);
        }
        first.iter_swap(last);
      }
    }
    return first;
  }

  template <typename TextStream>
  void to_string(TextStream &str, index_type from, index_type to) const {
    to = std::min(to, capacity());
    ++from;
    ++to;
    auto printer = [&](auto &arr) -> void {
      for (index_type i = from; i < ::std::min<index_type>(from + 1, to); ++i) {
        str << arr[i];
      }
      for (index_type i = from + 1; i < to; ++i) {
        str << ", " << arr[i];
      }
      str << "\n";
    };

    str << ">---------------\n";
    str << "size:    " << size() << "\n";
    str << "htoi:    ";
    printer(htoi);
    str << "itoh:    ";
    printer(itoh);
    str << "storage: ";
    printer(storage);
    str << "---------------<\n";
  }
};
}
}
