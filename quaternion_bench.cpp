#include "quaternion.hpp"

#include <benchmark/benchmark.h>

using namespace yaes::math;

static void escape(void *p) { asm volatile("" : : "g"(p) : "memory"); }
static void clobber() { asm volatile("" : : : "memory"); }

static void bm_from_axis_angle(benchmark::State &state) {
  double angle{1};
  vec3d axis{1, 0, 0};
  while (state.KeepRunning()) {
    quatd q = quatd::from_axis_angle(axis, angle);
    escape(&q);
    clobber();
  }
}
BENCHMARK(bm_from_axis_angle)->MinTime(1);

static void bm_to_axis_angle(benchmark::State &state) {
  double angle;
  vec3d axis;
  quatd q{1, 1, 1, 1};
  while (state.KeepRunning()) {
    q.to_axis_angle(axis, angle);
    escape(&angle);
    escape(&axis);
    clobber();
  }
}
BENCHMARK(bm_to_axis_angle)->MinTime(1);

static void bm_from_angles(benchmark::State &state) {
  vec3d angles{1, 2, 3};
  while (state.KeepRunning()) {
    quatd q = quatd::from_angles(angles);
    escape(&q);
    clobber();
  }
}
BENCHMARK(bm_from_angles)->MinTime(1);

static void bm_to_angles(benchmark::State &state) {
  quatd q{1, 1, 1, 1};
  while (state.KeepRunning()) {
    vec3d angles = q.to_angles();
    escape(&angles);
    clobber();
  }
}
BENCHMARK(bm_to_angles)->MinTime(1);

static void bm_to_matrix(benchmark::State &state) {
  quatd q{1, 1, 1, 1};
  while (state.KeepRunning()) {
    mat4d m = q.to_matrix();
    escape(&m);
    clobber();
  }
}
BENCHMARK(bm_to_matrix)->MinTime(1);

static void bm_quaternion_multiplication(benchmark::State &state) {
  quatd q1{6, 5, 4, 3};
  quatd q2{1, 2, 3, 4};
  while (state.KeepRunning()) {
    quatd q = q1 * q2;
    escape(&q);
    clobber();
  }
}
BENCHMARK(bm_quaternion_multiplication)->MinTime(1);

static void bm_rotate(benchmark::State &state) {
  quatd q{1, 2, 3, 4};
  vec4d v{1, 0, 0, 0};
  q.normalise();
  while (state.KeepRunning()) {
    vec4d vv = q * v;
    escape(&vv);
    clobber();
  }
}
BENCHMARK(bm_rotate)->MinTime(1);

BENCHMARK_MAIN();
