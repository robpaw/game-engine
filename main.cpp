#include <unistd.h>
#include <cstdarg>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>

#include <vector>
#include <thread>
#include <atomic>
#include <iostream>
#include <string>
#include <fstream>

#include "array.hpp"
#include "matrix.hpp"
#include "camera.hpp"
#include "transform.hpp"

using namespace std;

using vec3 = yaes::math::vec3f;
using vec4 = yaes::math::vec4f;
using mat4 = yaes::math::mat4f;
template <typename T> using arr = yaes::util::array<T>;
using hdl = yaes::util::handle<>;

#define perrf(...) print(print_errno | print_abort, __VA_ARGS__)
#define pmsgf(...) print(0, __VA_ARGS__)

enum { print_errno, print_abort };

void print(int flags, const char *format, ...) {
  char buffer[513];
  size_t len = 512;
  char *buf = buffer;
  int sts, errn = errno;
  va_list va;

  buf[len] = 0;

  if ((sts = snprintf(buf, len, "%012llu ",
                      (long long unsigned)(glfwGetTime() * 1.0e6))) < 0) {
    return;
  }
  len -= sts;
  buf += sts;

  va_start(va, format);
  sts = vsnprintf(buf, 512, format, va);
  va_end(va);
  if (sts < 0) {
    return;
  }
  len -= sts;
  buf += sts;

  if (flags & print_errno) {
    if ((sts = snprintf(buf, len, "; %d: %s", errn, strerror(errn))) < 0) {
      return;
    }
    len -= sts;
    buf += sts;
  }

  puts(buffer);

  if (flags & print_abort) {
    abort();
  }
}

void compileShader(GLuint shaderID, const string &source) {
  char const *src = source.c_str();
  glShaderSource(shaderID, 1, &src, NULL);
  glCompileShader(shaderID);

  GLint result = GL_FALSE;
  int infoLogLength;

  glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
  glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
  if (infoLogLength > 1) {
    vector<char> shaderErrorMessage(infoLogLength);
    glGetShaderInfoLog(shaderID, infoLogLength, NULL, &shaderErrorMessage[0]);
    pmsgf("Errors: %s", &shaderErrorMessage[0]);
  }
}

std::string loadFile(const string &path) {
  string content;
  ifstream stream(path, std::ios::in);
  stream.exceptions(std::ifstream::badbit);
  try {
    string line;
    while (getline(stream, line)) {
      content += line;
      content += '\n';
    }
  } catch (...) {
    pmsgf("Errors: cannot read %s", path.c_str());
    return "";
  }
  return content;
}

GLuint loadShaders(const char *vertex_sh, const char *fragment_sh) {

  GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
  GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

  pmsgf("Loading vertex shader %s...", vertex_sh);
  std::string vertexShaderCode = loadFile(vertex_sh);
  pmsgf("Compiling vertex shader %s...", vertex_sh);
  compileShader(vertexShaderID, vertexShaderCode);

  pmsgf("Loading fragment shader %s...", fragment_sh);
  std::string fragmentShaderCode = loadFile(fragment_sh);
  pmsgf("Compiling fragment shader %s...", fragment_sh);
  compileShader(fragmentShaderID, fragmentShaderCode);

  pmsgf("Linking program...");
  GLuint programID = glCreateProgram();
  glAttachShader(programID, vertexShaderID);
  glAttachShader(programID, fragmentShaderID);
  glLinkProgram(programID);

  GLint result = GL_FALSE;
  int infoLogLength;

  glGetProgramiv(programID, GL_LINK_STATUS, &result);
  glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
  if (infoLogLength > 1) {
    std::vector<char> programErrorMessage(std::max(infoLogLength, 1));
    glGetProgramInfoLog(programID, infoLogLength, NULL,
                        &programErrorMessage[0]);
    pmsgf("Errors: %s", &programErrorMessage[0]);
  }

  glDeleteShader(vertexShaderID);
  glDeleteShader(fragmentShaderID);

  return programID;
}

bool keys[1024] = {false};

void keyCallback(GLFWwindow *window, int key, int scancode, int action,
                 int mods) {
  if (action == GLFW_PRESS) {
    keys[key] = true;
  } else if (action == GLFW_RELEASE) {
    keys[key] = false;
  }
}

vec3 updateCameraPos(const vec3 &cameraPos, const vec3 &cameraFront,
                     const vec3 &cameraUp) {
  GLfloat cameraSpeed = 0.1f;
  vec3 res = cameraPos;
  if (keys[GLFW_KEY_W]) {
    res += cameraSpeed * cameraFront;
  }
  if (keys[GLFW_KEY_S]) {
    res -= cameraSpeed * cameraFront;
  }
  if (keys[GLFW_KEY_A]) {
    res -= yaes::math::normalise(yaes::math::cross(cameraFront, cameraUp)) *
           cameraSpeed;
  }
  if (keys[GLFW_KEY_D]) {
    res += yaes::math::normalise(yaes::math::cross(cameraFront, cameraUp)) *
           cameraSpeed;
  }
  return res;
}

void updateFPS(GLFWwindow *window, double draw_sec, size_t objs) {
  static double timestamp = glfwGetTime();
  static double frames = 0;

  frames += 1.0;

  double currentTime = glfwGetTime();
  double delta = currentTime - timestamp;
  if (delta > 1.0) {
    char buffer[256];
    snprintf(buffer, 256, "FPS: %.1f / objects: %llu / draw: %.3fms",
             frames / delta, objs, draw_sec * 1000.0);
    glfwSetWindowTitle(window, buffer);
    timestamp = currentTime;
    frames = 0;
  }
}

struct ModelAsset {
  GLuint program;
  GLuint texture;
  GLuint vbo;
  GLuint vao;
  GLuint model;
  GLuint view_proj;
  struct {
    GLenum type;
    GLint start;
    GLint count;
  } draw;
};

ModelAsset *create_box_asset() {
  static ModelAsset asset;
  static bool init = false;
  static GLfloat vertices[] = {
      -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 0.5f,  -0.5f, -0.5f, 1.0f, 0.0f,
      0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, 0.5f,  0.5f,  -0.5f, 1.0f, 1.0f,
      -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f, -0.5f, -0.5f, -0.5f, 0.0f, 0.0f,

      -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, 0.5f,  -0.5f, 0.5f,  1.0f, 0.0f,
      0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
      -0.5f, 0.5f,  0.5f,  0.0f, 1.0f, -0.5f, -0.5f, 0.5f,  0.0f, 0.0f,

      -0.5f, 0.5f,  0.5f,  1.0f, 0.0f, -0.5f, 0.5f,  -0.5f, 1.0f, 1.0f,
      -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
      -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, -0.5f, 0.5f,  0.5f,  1.0f, 0.0f,

      0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.5f,  0.5f,  -0.5f, 1.0f, 1.0f,
      0.5f,  -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,  -0.5f, -0.5f, 0.0f, 1.0f,
      0.5f,  -0.5f, 0.5f,  0.0f, 0.0f, 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

      -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,  -0.5f, -0.5f, 1.0f, 1.0f,
      0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, 0.5f,  -0.5f, 0.5f,  1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,

      -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f, 0.5f,  0.5f,  -0.5f, 1.0f, 1.0f,
      0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
      -0.5f, 0.5f,  0.5f,  0.0f, 0.0f, -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f};

  assert(!init);
  init = true;

  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
                        (void *)0);
  glEnableVertexAttribArray(0);

  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
                        (void *)(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(1);

  glBindVertexArray(0);

  GLuint program = loadShaders("shader.vs", "shader.fs");

  GLuint view_proj = glGetUniformLocation(program, "view_proj");
  if (view_proj == -1)
    perrf("uniform view_proj not found");
  GLuint model = glGetUniformLocation(program, "model");
  if (model == -1)
    perrf("uniform model not found");

  int width, height;
  unsigned char *image =
      SOIL_load_image("container.jpg", &width, &height, 0, SOIL_LOAD_RGB);

  GLuint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
               GL_UNSIGNED_BYTE, image);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
  SOIL_free_image_data(image);

  asset.program = program;
  asset.texture = texture;
  asset.vbo = vbo;
  asset.vao = vao;
  asset.model = model;
  asset.view_proj = view_proj;
  asset.draw.type = GL_TRIANGLES;
  asset.draw.start = 0;
  asset.draw.count = 36;

  return &asset;
}

hdl create_handle() {
  static uint16_t idx = 0;
  assert(idx < std::numeric_limits<uint16_t>::max());
  return {idx++};
}

using assets_t = arr<ModelAsset *>;
using transforms_t = arr<mat4>;

assets_t assets;
transforms_t transforms;

hdl create_box(const mat4 &transform) {
  static ModelAsset *asset = create_box_asset();
  auto h = create_handle();

  assets.insert(h, asset);
  transforms.insert(h, transform);

  return h;
}

int main(int argc, char **argv) {
  GLFWwindow *window = 0;

  pmsgf("Initialising glfw...");
  if (!glfwInit()) {
    pmsgf("Failed to initialise glfw");
    return -1;
  }

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_VERSION_MINOR, 2);
  //  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  //  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  pmsgf("Creating window...");
  window = glfwCreateWindow(1024, 768, "My window", NULL, NULL);
  if (window == nullptr) {
    pmsgf("Failed to create window");
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);

  pmsgf("Initialising glew...");
  glewExperimental = true;
  if (glewInit() != GLEW_OK) {
    pmsgf("Failed to initialise glew");
    glfwTerminate();
    return -1;
  }

  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
  glfwSetKeyCallback(window, &keyCallback);

  pmsgf("Starting event loop...");

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  // glEnable(GL_CULL_FACE);
  glClearColor(0.2, 0.2, 0.2, 1.0);

  vec3 cameraPosition{0, 0, 3};
  vec3 cameraFront{0, 0, -1};
  vec3 cameraUp{0, 1, 0};
  mat4 view;

  mat4 projection = yaes::camera::perspective<float>(45.0 * M_PI / 180.0,
                                                     4.0 / 3.0, 0.01, 1000);

  for (float x = -50; x < 50; x += 2) {
    for (float y = -50; y < 50; y += 2) {
      for (float z = -20; z < 0; z += 2) {
        mat4 model = yaes::math::transform::translate<float>(x, y, z);
        create_box(model);
      }
    }
  }

  do {
    double start, stop;
    start = glfwGetTime();

    view = yaes::camera::look_at<float>(cameraPosition,
                                        cameraPosition + cameraFront, cameraUp);

    mat4 VP = projection * view;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ModelAsset *asset = nullptr;
    size_t objs = 0;
    for (const auto &item : transforms) {
      ModelAsset *newasset = assets[item.handle];
      if (asset != newasset) {
        asset = newasset;
        glUseProgram(asset->program);
        glUniformMatrix4fv(asset->view_proj, 1, GL_FALSE, VP.data);
        glBindTexture(GL_TEXTURE_2D, asset->texture);
        glBindVertexArray(asset->vao);
      }
      glUniformMatrix4fv(asset->model, 1, GL_FALSE, item.value.data);
      glDrawArrays(asset->draw.type, asset->draw.start, asset->draw.count);
      ++objs;
    }
    glBindVertexArray(0);

    stop = glfwGetTime();
    updateFPS(window, stop - start, objs);

    glfwSwapBuffers(window);
    glfwPollEvents();
    cameraPosition = updateCameraPos(cameraPosition, cameraFront, cameraUp);

    usleep(1);
  } while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           !glfwWindowShouldClose(window));

  glfwTerminate();

  return 0;
}
