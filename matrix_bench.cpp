#include "matrix.hpp"

#include <benchmark/benchmark.h>

static void escape(void *p) { asm volatile("" : : "g"(p) : "memory"); }
static void clobber() { asm volatile("" : : : "memory"); }

typedef yaes::math::mat4d mat_t;
typedef yaes::math::vec4d vec_t;

static void bm_create(benchmark::State &state) {
  while (state.KeepRunning()) {
    mat_t m;
    escape(&m);
    (void)m;
  }
}
BENCHMARK(bm_create)->MinTime(1);

static void bm_mat_add(benchmark::State &state) {
  mat_t m1 = mat_t::ones();
  mat_t m2 = mat_t::ones();
  while (state.KeepRunning()) {
    mat_t m = m1 + m2;
    escape(&m);
    clobber();
  }
}
BENCHMARK(bm_mat_add)->MinTime(1);

static void bm_copy(benchmark::State &state) {
  mat_t m1 = mat_t::ones();
  mat_t m2;
  while (state.KeepRunning()) {
    m2 = m1;
    escape(&m1);
    clobber();
  }
}
BENCHMARK(bm_copy)->MinTime(1);

static void bm_scale2(benchmark::State &state) {
  vec_t v = {1, 2, 3, 4};
  while (state.KeepRunning()) {
    auto l = v.scale2();
    escape(&l);
    clobber();
  }
}
BENCHMARK(bm_scale2)->MinTime(1);

static void bm_scale(benchmark::State &state) {
  vec_t v = {1, 2, 3, 4};
  while (state.KeepRunning()) {
    auto l = v.scale();
    escape(&l);
    clobber();
  }
}
BENCHMARK(bm_scale)->MinTime(1);

static void bm_compare(benchmark::State &state) {
  mat_t m1 = mat_t::ones();
  mat_t m2 = mat_t::ones();
  while (state.KeepRunning()) {
    bool res = m1 == m2;
    escape(&res);
    clobber();
  }
}
BENCHMARK(bm_compare)->MinTime(1);

static void bm_matmat_mul(benchmark::State &state) {
  mat_t m1(1);
  mat_t m2(1);
  while (state.KeepRunning()) {
    auto m = m1 * m2;
    escape(&m);
    clobber();
  }
}
BENCHMARK(bm_matmat_mul)->MinTime(1);

static void bm_matvec_mul(benchmark::State &state) {
  mat_t m(1);
  vec_t v{1, 2, 3, 4};
  while (state.KeepRunning()) {
    auto mm = m * v;
    escape(&mm);
    clobber();
  }
}
BENCHMARK(bm_matvec_mul)->MinTime(1);

BENCHMARK_MAIN();
