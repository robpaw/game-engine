#pragma once

#include <cassert>
#include <cmath>
#include <cstring>
#include <array>
#include <initializer_list>
#include <type_traits>

namespace yaes {
namespace math {

template <typename T, size_t Rows, size_t Cols> struct matrix_storage {
  T data[Rows * Cols];
};
template <typename T> struct matrix_storage<T, 1, 3> {
  union {
    T data[3];
    struct {
      T x;
      T y;
      T z;
    };
  };
  inline constexpr matrix_storage() {}
  inline constexpr matrix_storage(T x, T y, T z) : x(x), y(y), z(z) {}
};
template <typename T> struct matrix_storage<T, 3, 1> {
  union {
    T data[3];
    struct {
      T x;
      T y;
      T z;
    };
  };
  inline constexpr matrix_storage() {}
  inline constexpr matrix_storage(T x, T y, T z) : x(x), y(y), z(z) {}
};
template <typename T> struct matrix_storage<T, 4, 4> {
  union {
    T data[16];
    struct {
      T m11;
      T m21;
      T m31;
      T m41;

      T m12;
      T m22;
      T m32;
      T m42;

      T m13;
      T m23;
      T m33;
      T m43;

      T m14;
      T m24;
      T m34;
      T m44;
    };
  };
  inline constexpr matrix_storage() {}
  inline constexpr matrix_storage(T m11, T m21, T m31, T m41, T m12, T m22,
                                  T m32, T m42, T m13, T m23, T m33, T m43,
                                  T m14, T m24, T m34, T m44)
      : m11(m11), m21(m21), m31(m31), m41(m41), m12(m12), m22(m22), m32(m32),
        m42(m42), m13(m13), m23(m23), m33(m33), m43(m43), m14(m14), m24(m24),
        m34(m34), m44(m44) {}
};
template <typename T> struct matrix_storage<T, 1, 4> {
  union {
    T data[4];
    struct {
      T x;
      T y;
      T z;
      T w;
    };
  };
  inline constexpr matrix_storage() {}
  inline constexpr matrix_storage(T x, T y, T z, T w)
      : x(x), y(y), z(z), w(w) {}
};
template <typename T> struct matrix_storage<T, 4, 1> {
  union {
    T data[4];
    struct {
      T x;
      T y;
      T z;
      T w;
    };
  };
  inline constexpr matrix_storage() {}
  inline constexpr matrix_storage(T x, T y, T z, T w)
      : x(x), y(y), z(z), w(w) {}
};

template <typename T, size_t Rows, size_t Cols>
struct matrix : matrix_storage<T, Rows, Cols> {
  typedef T value_type;
  typedef matrix_storage<T, Rows, Cols> base_type;
  typedef matrix<T, Rows, Cols> this_type;
  typedef size_t index_type;
  enum : index_type { rows = Rows, cols = Cols, size = Rows * Cols };
  constexpr static bool is_vector = rows == 1 || cols == 1;
  constexpr static bool is_square = rows == cols;
  constexpr static const T eps = 1.0e-9;

  using base_type::data;

  static this_type zeros() {
    this_type m;
    memset(m.data, 0, size * sizeof(T));
    return m;
  }
  static this_type ones() {
    static_assert(!is_vector, "not defined for vectors");
    this_type m;
    for (index_type i = 0; i < size; ++i) {
      m[i] = 1;
    }
    return m;
  }

  using base_type::matrix_storage;

  inline constexpr matrix() {}
  inline explicit matrix(T diagonal) {
    static_assert(is_square, "defined only for square-matrices");
    memset(data, 0, size * sizeof(T));
    for (index_type i = 0; i < size; i += (Cols + 1)) {
      data[i] = diagonal;
    }
  }

  inline constexpr T &operator()(index_type row, index_type col) {
    return data[col * Rows + row];
  }
  inline constexpr T operator()(index_type row, index_type col) const {
    return data[col * Rows + row];
  }

  template <typename Fn> inline constexpr this_type &foreach (Fn fn) {
    for (index_type i = 0; i < size; ++i) {
      fn(i, data[i]);
    }
    return *this;
  }
  template <typename Fn>
  inline constexpr const this_type &foreach (Fn fn) const {
    for (index_type i = 0; i < size; ++i) {
      fn(i, data[i]);
    }
    return *this;
  }

  inline constexpr this_type &operator+=(const this_type &other) {
    return foreach ([&](auto i, auto &v) { v += other[i]; });
  }
  inline constexpr this_type &operator+=(T val) {
    return foreach ([=](auto i, auto &v) { v += val; });
  }
  inline constexpr this_type &operator-=(const this_type &other) {
    return foreach ([&](auto i, auto &v) { v -= other[i]; });
  }
  inline constexpr this_type &operator-=(T val) {
    return foreach ([=](auto i, auto &v) { v -= val; });
  }
  inline constexpr this_type &operator*=(T val) {
    return foreach ([=](auto i, auto &v) { v *= val; });
  }
  inline constexpr this_type &operator/=(T val) {
    return operator*=(1.0 / val);
  }
  inline constexpr T &operator[](index_type i) { return data[i]; }
  inline constexpr T operator[](index_type i) const { return data[i]; }

  inline constexpr T scale2() const {
    T sum{0};
    foreach ([&](auto i, auto v) { sum += v * v; })
      ;
    return sum;
  }

  inline constexpr T scale() const { return std::sqrt(scale2()); }

  inline constexpr void normalise() {
    T l = scale2();
    if (fabs(l - 1) > eps) {
      l = std::sqrt(l);
      foreach ([=](auto i, auto &v) { v /= l; })
        ;
    }
  }
  inline constexpr bool is_normalised() const {
    return fabs(scale2() - 1) <= eps;
  }

  inline bool operator==(const this_type &other) const {
    for (index_type i = 0; i < size; ++i) {
      if (fabs(data[i] - other.data[i]) > eps)
        return false;
    }
    return true;
  }
  inline bool operator!=(const this_type &other) const {
    return !operator==(other);
  }

  template <typename Stream> void to_string(Stream &s) {
    for (index_type r = 0; r < rows; ++r) {
      for (index_type c = 0; c < std::min<index_type>(cols, 1); ++c) {
        s << operator()(r, c);
      }
      for (index_type c = 1; c < cols; ++c) {
        s << ", " << operator()(r, c);
      }
      s << '\n';
    }
  }
};

template <typename T> using vec3 = matrix<T, 3, 1>;
template <typename T> using vec4 = matrix<T, 4, 1>;

template <typename T> using mat3 = matrix<T, 3, 3>;
template <typename T> using mat4 = matrix<T, 4, 4>;

using vec3f = vec3<float>;
using vec3d = vec3<double>;
using vec4f = vec4<float>;
using vec4d = vec4<double>;

using mat3f = mat3<float>;
using mat3d = mat3<double>;
using mat4f = mat4<float>;
using mat4d = mat4<double>;

template <typename T, size_t R, size_t C>
inline matrix<T, R, C> normalise(const matrix<T, R, C> & mat) {
    matrix<T, R, C> m(mat);
    m.normalise();
    return m;
}

template <typename T, size_t R, size_t C, typename Mat = matrix<T, R, C>>
inline constexpr Mat operator+(const matrix<T, R, C> &m1,
                               const matrix<T, R, C> &m2) {
  Mat m(m1);
  m += m2;
  return m;
}

template <typename T, size_t R, size_t C, typename Mat = matrix<T, R, C>>
inline constexpr Mat operator-(const matrix<T, R, C> &m1,
                               const matrix<T, R, C> &m2) {
  Mat m(m1);
  m -= m2;
  return m;
}

template <typename T, size_t R, size_t C, typename Mat = matrix<T, R, C>>
inline constexpr Mat operator+(const matrix<T, R, C> &m1, T val) {
  Mat m(m1);
  m += val;
  return m;
}
template <typename T, size_t R, size_t C, typename Mat = matrix<T, R, C>>
inline constexpr Mat operator+(T val, const matrix<T, R, C> &m1) {
  return m1 + val;
}

template <typename T, size_t R, size_t C, typename Mat = matrix<T, R, C>>
inline constexpr Mat operator-(const matrix<T, R, C> &m1, T val) {
  Mat m(m1);
  m -= val;
  return m;
}
template <typename T, size_t R, size_t C, typename Mat = matrix<T, R, C>>
inline constexpr Mat operator-(T val, const matrix<T, R, C> &m1) {
  return m1 - val;
}

template <typename T, size_t R, size_t C, typename Mat = matrix<T, R, C>>
inline constexpr Mat operator*(const matrix<T, R, C> &m1, T val) {
  Mat m(m1);
  m *= val;
  return m;
}
template <typename T, size_t R, size_t C, typename Mat = matrix<T, R, C>>
inline constexpr Mat operator*(T val, const matrix<T, R, C> &m1) {
  return m1 * val;
}

template <typename T, size_t R, size_t C, typename Mat = matrix<T, R, C>>
inline constexpr Mat operator/(const matrix<T, R, C> &m1, T val) {
  Mat m(m1);
  m /= val;
  return m;
}
template <typename T, size_t R, size_t C, typename Mat = matrix<T, R, C>>
inline constexpr Mat operator/(T val, const matrix<T, R, C> &m1) {
  return m1 / val;
}

template <typename T, size_t R, size_t CR, size_t C>
constexpr std::enable_if_t<R != 4 || CR != 4 || C != 4, matrix<T, R, C>>
operator*(const matrix<T, R, CR> &m1, const matrix<T, CR, C> &m2) {
  typedef matrix<T, R, C> Mat;
  typedef typename Mat::index_type index_type;
  typedef typename Mat::value_type value_type;

  Mat m;
  for (index_type r = 0; r < R; ++r) {
    for (index_type c = 0; c < C; ++c) {
      value_type sum{0};
      for (index_type cr = 0; cr < CR; ++cr) {
        sum += m1(r, cr) * m2(cr, c);
      }
      m(r, c) = sum;
    }
  }
  return m;
}

template <typename T, size_t R, size_t CR, size_t C>
constexpr std::enable_if_t<R == 4 && CR == 4 && C == 4, matrix<T, R, C>>
operator*(const matrix<T, R, CR> &m1, const matrix<T, CR, C> &m2) {
  return {
      m1.m11 * m2.m11 + m1.m12 * m2.m21 + m1.m13 * m2.m31 + m1.m14 * m2.m41,
      m1.m21 * m2.m11 + m1.m22 * m2.m21 + m1.m23 * m2.m31 + m1.m24 * m2.m41,
      m1.m31 * m2.m11 + m1.m32 * m2.m21 + m1.m33 * m2.m31 + m1.m34 * m2.m41,
      m1.m41 * m2.m11 + m1.m42 * m2.m21 + m1.m43 * m2.m31 + m1.m44 * m2.m41,
      m1.m11 * m2.m12 + m1.m12 * m2.m22 + m1.m13 * m2.m32 + m1.m14 * m2.m42,
      m1.m21 * m2.m12 + m1.m22 * m2.m22 + m1.m23 * m2.m32 + m1.m24 * m2.m42,
      m1.m31 * m2.m12 + m1.m32 * m2.m22 + m1.m33 * m2.m32 + m1.m34 * m2.m42,
      m1.m41 * m2.m12 + m1.m42 * m2.m22 + m1.m43 * m2.m32 + m1.m44 * m2.m42,
      m1.m11 * m2.m13 + m1.m12 * m2.m23 + m1.m13 * m2.m33 + m1.m14 * m2.m43,
      m1.m21 * m2.m13 + m1.m22 * m2.m23 + m1.m23 * m2.m33 + m1.m24 * m2.m43,
      m1.m31 * m2.m13 + m1.m32 * m2.m23 + m1.m33 * m2.m33 + m1.m34 * m2.m43,
      m1.m41 * m2.m13 + m1.m42 * m2.m23 + m1.m43 * m2.m33 + m1.m44 * m2.m43,
      m1.m11 * m2.m14 + m1.m12 * m2.m24 + m1.m13 * m2.m34 + m1.m14 * m2.m44,
      m1.m21 * m2.m14 + m1.m22 * m2.m24 + m1.m23 * m2.m34 + m1.m24 * m2.m44,
      m1.m31 * m2.m14 + m1.m32 * m2.m24 + m1.m33 * m2.m34 + m1.m34 * m2.m44,
      m1.m41 * m2.m14 + m1.m42 * m2.m24 + m1.m43 * m2.m34 + m1.m44 * m2.m44};
}

template <typename T, size_t R, size_t C>
inline constexpr T dot(const matrix<T, R, C> &v1, const matrix<T, R, C> &v2) {
  typedef matrix<T, R, C> Mat;
  typedef typename Mat::index_type index_type;

  static_assert(Mat::is_vector, "defined only for vectors");

  T sum{0};
  for (index_type i = 0; i < Mat::size; ++i) {
    sum += v1[i] * v2[i];
  }
  return sum;
}

template <typename T, size_t R, size_t C>
inline constexpr matrix<T, R, C> cross(const matrix<T, R, C> &v1,
                                       const matrix<T, R, C> &v2) {
  typedef matrix<T, R, C> Mat;
  static_assert((R == 3 || C == 3) && Mat::is_vector, "defined only for vectors of 3 elements");
  return {v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z,
          v1.x * v2.y - v1.y * v2.x};
}
}
}
