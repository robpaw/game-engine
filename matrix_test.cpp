
#include "matrix.hpp"
#include <iostream>
#include <gtest/gtest.h>

#define ARRSIZ(arr) sizeof(arr) / sizeof(*arr)

namespace {

typedef yaes::math::vec4d vec_t;
typedef yaes::math::mat4d mat_t;
typedef yaes::math::mat4d::index_type idx_t;

using namespace std;
using namespace yaes::math;

TEST(MatrixTest, Init) {
  auto m1 = mat_t::ones();
  for (idx_t i = 0; i < mat_t::size; ++i) {
    ASSERT_EQ(m1[i], 1.0);
  }
  auto m2 = mat_t::zeros();
  for (idx_t i = 0; i < mat_t::size; ++i) {
    ASSERT_EQ(m2[i], 0.0);
  }
  mat_t m3(0);
  ASSERT_EQ(m3, m2);
}

TEST(MatrixTest, Multiplication) {
  mat_t m1(1);
  mat_t m2(1);
  mat_t m3(1);
  ASSERT_EQ(m1 * m2, m3);

  vec_t v1{1, 2, 3, 4};
  vec_t v2 = m1 * v1;

  ASSERT_EQ(v2, v1);
}

TEST(MatrixTest, Operators) {
  mat_t m1(1);
  mat_t m2(1);
  mat_t m3 = m1 - m2;
  mat_t m4 = mat_t::zeros();

  mat_t m5 = m1 + m2;
  mat_t m6(2);

  ASSERT_EQ(m3, m4);
  ASSERT_EQ(m5, m6);

  mat_t m7(1);
  mat_t m8 = m7 * 2.0;

  ASSERT_EQ(m8, m6);
}

TEST(MatrixTest, DotProduct) {
  vec_t v1 = {1, 2, 3, 4};
  vec_t v2 = {2, 3, 4, 1};

  auto d = dot(v1, v2);

  ASSERT_EQ(d, 24.0);
}

TEST(MatrixTest, CrossProduct) {
  typedef yaes::math::vec3d vec_t;
  vec_t v1 = {0, 0, -1};
  vec_t v2 = {0, 1, 0};
  vec_t v3 = {1, 0, 0};

  vec_t v4 = cross(v1, v2);

  ASSERT_EQ(v4, v3);

  vec_t v5 = cross(v4, v1);

  ASSERT_EQ(v5, v2);
}

TEST(MatrixTest, Misc) {
  vec_t v1 = {2, 0, 0, 0};
  vec_t v2 = {0, 2, 0, 0};
  vec_t v3 = {0, 0, 2, 0};
  vec_t v4 = {0, 0, 0, 2};

  ASSERT_EQ(v1.scale(), 2);
  ASSERT_EQ(v2.scale(), 2);
  ASSERT_EQ(v3.scale(), 2);
  ASSERT_EQ(v4.scale(), 2);

  ASSERT_EQ(v1.x, 2);
  ASSERT_EQ(v2.y, 2);
  ASSERT_EQ(v3.z, 2);
  ASSERT_EQ(v4.w, 2);

  v1.normalise();
  ASSERT_EQ(v1.scale(), 1);

  vec_t v5 = {3, 4, 0, 0};
  ASSERT_EQ(v5.scale(), 5);
  v5.normalise();
  ASSERT_EQ(v5.scale(), 1);
}
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
