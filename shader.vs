#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;

uniform mat4 view_proj;
uniform mat4 model;

out vec2 fragmentUV;

void main(){
    gl_Position = view_proj * model * vec4(position, 1);
    fragmentUV = uv;
}
