#pragma once

#include "matrix.hpp"

namespace yaes {
namespace math {
namespace transform {

template<typename T>
using vec3 = ::yaes::math::vec3<T>;
template<typename T>
using vec4 = ::yaes::math::vec4<T>;
template<typename T>
using mat4 = ::yaes::math::mat4<T>;

template <typename T> inline mat4<T> rotate(T p, T y, T r) {
  T sp = std::sin(p);
  T sy = std::sin(y);
  T sr = std::sin(r);
  T cp = std::cos(p);
  T cy = std::cos(y);
  T cr = std::cos(r);

  return {cy * cr, sr, -sy * cr, 0, sy * sp - cy * sr * cp, cr * cp,
          sy * sr * cp + cy * sp, 0, cy * sr * sp + sy * cp, -cr * sp,
          cy * cp - sy * sr * sp, 0, 0, 0, 0, 1};
}
template <typename T> inline mat4<T> rotate(const vec3<T> &angles) {
  return rotate(angles.x, angles.y, angles.z);
}
template <typename T> inline mat4<T> rotate(T x, T y, T z, T angle) {
  T c = std::cos(angle);
  T s = std::sin(angle);

  T xx = x * x;
  T yy = y * y;
  T zz = z * z;

  T t = 1 - c;

  return {t * xx + c, t * x * y + z * s, t * x * z - y * s, 0,
          t * x * y - z * s, t * yy + c, t * y * z + x * s, 0,
          t * x * z + y * s, t * y * z - x * s, t * zz + c, 0, 0, 0, 0, 1};
}
template <typename T> inline mat4<T> rotate(const vec3<T> &axis, T angle) {
  return rotate(axis.x, axis.y, axis.z, angle);
}
template <typename T> inline mat4<T> translate(T dx, T dy, T dz) {
  mat4<T> m(1);
  m.m14 = dx;
  m.m24 = dy;
  m.m34 = dz;
  return m;
}
template <typename T> inline mat4<T> translate(const vec3<T> &d) {
  return translate(d.x, d.y, d.z);
}
template <typename T> inline mat4<T> scale(T sx, T sy, T sz) {
  mat4<T> m = mat4<T>::zeros();
  m.m11 = sx;
  m.m22 = sy;
  m.m33 = sz;
  return m;
}
template <typename T> inline mat4<T> scale(const vec3<T> s) {
  return scale(s.x, s.y, s.z);
}
template <typename T> inline mat4<T> scale(T s) {
  return mat4<T>(s);
}
}
}
}
