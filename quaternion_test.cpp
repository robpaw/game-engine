
#include "quaternion.hpp"
#include "transform.hpp"
#include <iostream>
#include <gtest/gtest.h>

namespace {

typedef yaes::math::mat4d mat_t;
typedef yaes::math::vec4d vec_t;
typedef yaes::math::quatd quat_t;
typedef yaes::math::quatd::index_type idx_t;
typedef yaes::math::quatd::value_type val_t;

using namespace std;
using namespace yaes::math;

TEST(QuaternionTest, Create) {
  quat_t q1{0.5, 0.5, 0.5, 0.5};
  mat_t m1 = q1.to_matrix();

  mat_t m2(0);
  m2.m21 = 1;
  m2.m32 = 1;
  m2.m13 = 1;
  m2.m44 = 1;

  ASSERT_EQ(m1, m2);
}

TEST(QuaternionTest, FromAnglesToAnglesAndToMatrix) {
  val_t ph = M_PI * 0.5 - 0.00001;
  for (val_t p = -ph; p <= ph; p += 0.1) {
    for (val_t y = -ph; y <= ph; y += 0.1) {
      for (val_t r = -ph; r <= ph; r += 0.1) {
        vec3d a1{p, y, r};
        quat_t q = quat_t::from_angles(a1);
        vec3d a2 = q.to_angles();

        ASSERT_EQ(a2, a1);

        mat_t m1 = q.to_matrix();
        mat_t m2 = transform::rotate(a1);

        ASSERT_EQ(m2, m1);
      }
    }
  }
}

TEST(QuaternionTest, FromAxisAngleToAxisAngleToMatrix) {
  for (val_t a = 0.0; a < M_PI; a += 0.1) {
    for (val_t x = -1.0; x <= 1.0; x += 0.1) {
      for (val_t y = -1.0; y <= 1.0; y += 0.1) {
        for (val_t z = -1.0; z <= 1.0; z += 0.1) {
          val_t angle1{a}, angle2;
          vec3d axis1{x, y, z}, axis2;
          axis1.normalise();
          quat_t q = quat_t::from_axis_angle(axis1, angle1);
          q.to_axis_angle(axis2, angle2);

          ASSERT_FLOAT_EQ(angle2, angle1);
          ASSERT_EQ(axis2, axis1);

          mat_t v1 = transform::rotate(axis1, angle2);
          mat_t v2 = q.to_matrix();

          ASSERT_EQ(v2, v1);
        }
      }
    }
  }
}

TEST(QuaternionTest, QuaternionMultiplication) {
  {
    quat_t q1{1, 1, 1, 1};
    quat_t q2{1, 1, 1, 1};
    quat_t q3 = q1 * q2;
    quat_t q4{2, 2, 2, -2};
    ASSERT_EQ(q3, q4);
    quat_t q5 = q2 * q1;
    quat_t q6{2, 2, 2, -2};
    ASSERT_EQ(q5, q6);
  }
  {
    quat_t q1{1, 1, 1, 1};
    quat_t q2{0, 1, 0, 1};
    quat_t q3 = q1 * q2;
    quat_t q4{0, 2, 2, 0};
    ASSERT_EQ(q3, q4);
    quat_t q5 = q2 * q1;
    quat_t q6{2, 2, 0, 0};
    ASSERT_EQ(q5, q6);
  }
  {
    quat_t q1{1, 2, 3, 4};
    quat_t q2{6, 5, 4, 3};
    quat_t q3 = q1 * q2;
    quat_t q4{20, 40, 18, -16};
    ASSERT_EQ(q3, q4);
    quat_t q5 = q2 * q1;
    quat_t q6{34, 12, 32, -16};
    ASSERT_EQ(q5, q6);
  }
}

TEST(QuaternionTest, RotateVector) {
  val_t ph = M_PI * 0.5 - 0.1;
  for (val_t p = -ph; p <= ph; p += 0.1) {
    for (val_t y = -ph; y <= ph; y += 0.1) {
      for (val_t r = -ph; r <= ph; r += 0.1) {
        vec4d v{2, 0, 0, 1};

        vec3d angles{p, y, r};
        quat_t q = quat_t::from_angles(angles);
        mat_t m = transform::rotate(angles);

        ASSERT_EQ(q.to_matrix(), m);

        vec4d v1 = m * v;
        vec4d v2 = q * v;

        ASSERT_EQ(v2, v1);
      }
    }
  }
}
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
