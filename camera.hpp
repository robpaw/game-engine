#pragma once

#include <cassert>
#include <cmath>

#include "matrix.hpp"
#include "transform.hpp"

namespace yaes {
namespace camera {

template <typename T> using vec3 = ::yaes::math::vec3<T>;
template <typename T> using mat4 = ::yaes::math::mat4<T>;

template <typename T>
mat4<T> look_at(const vec3<T> &camera_position, const vec3<T> &target_position,
                const vec3<T> &camera_up) {
  auto camera_front = target_position - camera_position;
  camera_front.normalise();
  auto camera_right = yaes::math::cross(camera_front, camera_up);

  assert(camera_up.is_normalised());
  assert(yaes::math::cross(camera_right, camera_front) == camera_up);

  mat4<T> rotation{camera_right.x, camera_up.x, -camera_front.x, 0,
                   camera_right.y, camera_up.y, -camera_front.y, 0,
                   camera_right.z, camera_up.z, -camera_front.z, 0, 0, 0, 0, 1};
  mat4<T> translation = ::yaes::math::transform::translate(
      -camera_position.x, -camera_position.y, -camera_position.z);
  return rotation * translation;
}

template <typename T> mat4<T> perspective(T vfov, T aratio, T near, T far) {
  T ctg = 1.0 / std::tan(vfov * 0.5);
  T dplanes = far - near;

  assert(fabs(dplanes) > 0.001);

  T sx = ctg / aratio;
  T sy = ctg;
  T sz = -(near + far) / dplanes;
  T dz = -2 * far * near / dplanes;

  return {sx, 0, 0, 0, 0, sy, 0, 0, 0, 0, sz, -1, 0, 0, dz, 0};
}
}
}
