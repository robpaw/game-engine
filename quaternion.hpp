#pragma once

#include "matrix.hpp"

namespace yaes {
namespace math {

// OX - pitch, attitude
// OY - yaw, heading
// OZ - roll, bank

template <typename T> struct quaternion {
  union {
    T data[4];
    struct {
      T x;
      T y;
      T z;
      T w;
    };
  };

  typedef quaternion<T> this_type;
  typedef size_t index_type;
  typedef T value_type;

  constexpr static const T eps = 1.0e-9;

  inline constexpr quaternion() {}

  inline constexpr quaternion(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}

  inline static this_type from_vector(const vec4<T> &v) {
    return {v.x, v.y, v.z, v.w};
  }

  static this_type from_axis_angle(const vec3<T> &axis, T angle) {
    this_type q{axis.x, axis.y, axis.z, 0};
    angle *= 0.5;
    T sin_angle = std::sin(angle);
    T cos_angle = std::cos(angle);
    q *= sin_angle;
    q.w = cos_angle;

    return q;
  }
  static this_type from_angles(T p, T y, T r) {
    p *= 0.5;
    y *= 0.5;
    r *= 0.5;

    T sp = std::sin(p);
    T sy = std::sin(y);
    T sr = std::sin(r);
    T cp = std::cos(p);
    T cy = std::cos(y);
    T cr = std::cos(r);

    return {sy * sr * cp + cy * cr * sp, sy * cr * cp + cy * sr * sp,
            cy * sr * cp - sy * cr * sp, cy * cr * cp - sy * sr * sp};
  }
  inline static this_type from_angles(const vec3<T> &angles) {
    return from_angles(angles.x, angles.y, angles.z);
  }
  vec3<T> to_angles() {
    T x2 = x * x;
    T y2 = y * y;
    T z2 = z * z;
    return {atan2(2 * (x * w - y * z), 1 - 2 * (x2 + z2)),
            atan2(2 * (y * w - x * z), 1 - 2 * (y2 + z2)),
            asin(2 * (x * y + z * w))};
  }
  inline constexpr this_type conjugate() const { return {-x, -y, -z, w}; }
  inline constexpr this_type &operator*=(T val) {
    x *= val;
    y *= val;
    z *= val;
    w *= val;
    return *this;
  }
  inline constexpr this_type &operator/=(T val) {
    return operator*=(1.0 / val);
  }
  inline constexpr T scale2() const { return x * x + y * y + z * z + w * w; }

  inline constexpr T scale() const { return std::sqrt(scale2()); }

  inline constexpr void normalise() {
    T l = scale2();
    if (fabs(l - 1) > eps) {
      l = std::sqrt(l);
      operator*=(1.0 / l);
    }
  }
  inline constexpr bool is_normalised() const {
    return fabs(scale2() - 1) <= eps;
  }

  inline bool operator==(const this_type &other) const {
    for (index_type i = 0; i < 4; ++i) {
      if (fabs(data[i] - other.data[i]) > eps)
        return false;
    }
    return true;
  }
  inline bool operator!=(const this_type &other) const {
    return !operator==(other);
  }

  inline constexpr vec4<T> to_vector() const { return vec4<T>{x, y, z, w}; }

  mat4<T> to_matrix() const {
    this_type q(*this);
    q.normalise();

    T x2 = q.x * q.x;
    T y2 = q.y * q.y;
    T z2 = q.z * q.z;
    T xy = q.x * q.y;
    T xz = q.x * q.z;
    T yz = q.y * q.z;
    T wx = q.w * q.x;
    T wy = q.w * q.y;
    T wz = q.w * q.z;

    return {1.0 - 2.0 * (y2 + z2), 2.0 * (xy + wz), 2.0 * (xz - wy), 0,
            2.0 * (xy - wz), 1.0 - 2.0 * (x2 + z2), 2.0 * (yz + wx), 0,
            2.0 * (xz + wy), 2.0 * (yz - wx), 1.0 - 2.0 * (x2 + y2), 0, 0, 0, 0,
            1};
  }
  void to_axis_angle(vec3<T> &axis, T &angle) const {
    this_type q(*this);
    q.normalise();
    axis.x = q.x;
    axis.y = q.y;
    axis.z = q.z;
    axis *= (1.0 / std::sqrt(1.0 - q.w * q.w));
    angle = 2.0 * acos(q.w);
  }

  template <typename Stream> void to_string(Stream &s) {
    s << data[0];
    for (index_type i = 1; i < 4; ++i) {
      s << ", " << data[i];
    }
    s << '\n';
  }
};

template <typename T> using quat = quaternion<T>;
using quatd = quat<double>;
using quatf = quat<float>;

template <typename T>
constexpr quaternion<T> operator*(const quaternion<T> &q1,
                                  const quaternion<T> &q2) {
  return {q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y,
          q1.w * q2.y + q1.y * q2.w + q1.z * q2.x - q1.x * q2.z,
          q1.w * q2.z + q1.z * q2.w + q1.x * q2.y - q1.y * q2.x,
          q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z};
}

template <typename T>
constexpr vec4<T> operator*(const quaternion<T> &q, const vec4<T> &v) {
  quaternion<T> qp(q);
  qp.normalise();
  return (qp * (quaternion<T>::from_vector(v) * qp.conjugate())).to_vector();
}
}
}
