
#include "array.hpp"

#include <cassert>
#include <cmath>
#include <iostream>
#include <gtest/gtest.h>

#define ARRSIZ(arr) sizeof(arr) / sizeof(*arr)

namespace {

using namespace std;
using namespace yaes::util;

#define N 6

typedef ::yaes::util::array<int, N> arr_t;
typedef arr_t::index_type idx_t;
typedef arr_t::handle_type hdl_t;

void print(const arr_t &arr, idx_t n) { arr.to_string(cout, (idx_t)0, n); }

class ArrayTest : public ::testing::Test {
public:
  arr_t arr;

  hdl_t handle[N + 1] = {{0}, {1}, {2}, {3}, {4}, {5}, {6}};
  int value[N + 1] = {0, 1, 2, 3, 4, 5, 6};
  idx_t order[N + 1] = {0, 2, 1, 6, 3, 4, 5};

  static_assert(ARRSIZ(handle) == ARRSIZ(value), "invalid size");
  static_assert(ARRSIZ(value) == ARRSIZ(order), "invalid size");

  void SetUp() {
    for (idx_t i = 1; i <= N; ++i) {
      idx_t idx = order[i];
      ASSERT_TRUE(arr.insert(handle[idx], value[idx]));
    }
  }
  void TearDown() { arr.clear(); }
};

TEST_F(ArrayTest, Init) {
  ASSERT_TRUE(arr.full());
  ASSERT_EQ(N, arr.capacity());
  for (idx_t i = 1; i <= N; ++i) {
    ASSERT_EQ(value[order[i]], arr[i - 1]);
  }
}

TEST_F(ArrayTest, Copy) {
  ASSERT_TRUE(arr.full());
  ASSERT_EQ(N, arr.capacity());

  auto a = arr;
  for (idx_t i = 0; i < N; ++i) {
    ASSERT_EQ(arr[i], a[i]);
  }
  for (idx_t i = 1; i <= N; ++i) {
    ASSERT_EQ(arr[handle[i]], a[handle[i]]);
  }

  ASSERT_EQ(arr, a);
  ASSERT_TRUE(a.remove(handle[1]));
  ASSERT_NE(arr, a);
  ASSERT_TRUE(arr.remove(handle[1]));
  ASSERT_EQ(arr, a);
}

TEST_F(ArrayTest, Get) {
  ASSERT_TRUE(arr.full());
  for (idx_t i = 1; i <= N; ++i) {
    ASSERT_EQ(value[handle[i].value], arr[handle[i]]);
  }
}

TEST_F(ArrayTest, SetGet) {
  ASSERT_TRUE(arr.full());
  for (idx_t i = 1, vv = 101; i <= N; ++i, vv += 7) {
    ASSERT_TRUE(arr.set(handle[i], vv));
    ASSERT_EQ(vv, arr[handle[i]]);
    int v;
    arr.get(handle[i], v);
    ASSERT_EQ(vv, v);
  }
}

TEST_F(ArrayTest, Swap) {
  ASSERT_TRUE(arr.full());
  for (idx_t i = 0; i < N; ++i) {
    auto v = arr[i];
    arr.swap(i, i);
    ASSERT_EQ(arr[i], v);
  }
  for (idx_t i = 0, j = N - 1; i < j; ++i, --j) {
    auto vi = arr[i];
    auto vj = arr[j];
    arr.swap(i, j);
    ASSERT_EQ(arr[i], vj);
    ASSERT_EQ(arr[j], vi);
  }
}

#define REMOVE_TEST(EXPECTED_COUNT, H_TO_REMOVE)                               \
  {                                                                            \
    ASSERT_TRUE(arr.remove(H_TO_REMOVE));                                      \
    idx_t count = 0;                                                           \
    for (const auto &t : arr) {                                                \
      ASSERT_FALSE(H_TO_REMOVE == t.handle);                                   \
      ASSERT_EQ(value[t.handle.value], t.value);                               \
      ASSERT_EQ(arr[t.handle], t.value);                                       \
      ++count;                                                                 \
    }                                                                          \
    ASSERT_EQ(EXPECTED_COUNT, count);                                          \
    ASSERT_FALSE(arr.full());                                                  \
  }

#define INSERT_TEST(EXPECTED_COUNT, H_TO_INSERT)                               \
  {                                                                            \
    ASSERT_TRUE(arr.insert(H_TO_INSERT, value[H_TO_INSERT.value]));            \
    ASSERT_FALSE(arr.insert(H_TO_INSERT, value[H_TO_INSERT.value]));           \
    ASSERT_EQ(EXPECTED_COUNT, arr.size());                                     \
    idx_t count = 0;                                                           \
    for (const auto &t : arr) {                                                \
      ASSERT_EQ(value[t.handle.value], t.value);                               \
      ++count;                                                                 \
    }                                                                          \
    ASSERT_EQ(EXPECTED_COUNT, count);                                          \
  }

TEST_F(ArrayTest, Insert) {
  ASSERT_TRUE(arr.full());
  ASSERT_FALSE(arr.insert({6}, 6));

  arr.clear();
  ASSERT_TRUE(arr.empty());

  INSERT_TEST(1, handle[2]);
  INSERT_TEST(2, handle[5]);
  INSERT_TEST(3, handle[3]);
  INSERT_TEST(4, handle[0]);
  INSERT_TEST(5, handle[1]);
  INSERT_TEST(6, handle[4]);

  ASSERT_TRUE(arr.full());

  for (idx_t i = 0; i < N; ++i) {
    ASSERT_FALSE(arr.insert(handle[i], 10));
  }
  for (const auto &t : arr) {
    ASSERT_NE(10, t.value);
  }
}

TEST_F(ArrayTest, Remove) {
  ASSERT_TRUE(arr.full());

  REMOVE_TEST(N - 1, handle[2]);
  REMOVE_TEST(N - 2, handle[5]);
  REMOVE_TEST(N - 3, handle[3]);
  REMOVE_TEST(N - 4, handle[6]);
  REMOVE_TEST(N - 5, handle[1]);
  REMOVE_TEST(N - 6, handle[4]);

  ASSERT_TRUE(arr.empty());
  ASSERT_EQ(0, arr.size());

  for (idx_t i = 1; i <= N; ++i) {
    ASSERT_FALSE(arr.remove(handle[i]));
  }
}

TEST_F(ArrayTest, RemoveInsert) {
  ASSERT_TRUE(arr.full());

  REMOVE_TEST(N - 1, handle[2]);
  INSERT_TEST(N, handle[2]);
  REMOVE_TEST(N - 1, handle[5]);
  INSERT_TEST(N, handle[5]);
  REMOVE_TEST(N - 1, handle[3]);
  INSERT_TEST(N, handle[3]);
  REMOVE_TEST(N - 1, handle[6]);
  INSERT_TEST(N, handle[6]);
  REMOVE_TEST(N - 1, handle[1]);
  INSERT_TEST(N, handle[1]);
  REMOVE_TEST(N - 1, handle[4]);
  INSERT_TEST(N, handle[4]);

  ASSERT_TRUE(arr.full());
}

#undef INSERT_TEST
#undef REMOVE_TEST

#define PARTITION_TEST(a, pred, num)                                           \
  do {                                                                         \
    auto count = num;                                                          \
    auto part = a.partition(a.begin(), a.end(), pred);                         \
    for (auto it = a.begin(); it != part && count != 0; ++it) {                \
      ASSERT_TRUE(pred(*it));                                                  \
      --count;                                                                 \
    }                                                                          \
    for (auto it = part; it != a.end() && count != 0; ++it) {                  \
      ASSERT_TRUE(!pred(*it));                                                 \
      --count;                                                                 \
    }                                                                          \
  } while (0)

TEST_F(ArrayTest, Partition) {
  for (idx_t i = 1; i <= N; ++i) {
    for (idx_t k = 1; k <= i; ++k) {
      auto val = value[k];
      auto a = arr;

      auto pred1 = [&](const auto &v) { return v.value < val; };
      PARTITION_TEST(a, pred1, k);

      auto pred2 = [&](const auto &v) { return v.value > val; };
      PARTITION_TEST(a, pred2, k);

      auto pred3 = [&](const auto &v) { return v.value == val; };
      PARTITION_TEST(a, pred3, k);
    }
  }
}

#undef PARTITION_TEST

#define COUNT_HANDLES(arr, expected)                                           \
  do {                                                                         \
    idx_t count = 0;                                                           \
    for (auto h : handle) {                                                    \
      if (arr.contains(h)) {                                                   \
        ++count;                                                               \
        ASSERT_EQ(arr[h], h.value);                                            \
      }                                                                        \
    }                                                                          \
    ASSERT_EQ(count, expected);                                                \
  } while (0)

TEST_F(ArrayTest, Sort) {
  {
    auto a = arr;

    a.sort();

    ASSERT_EQ(1, a[0]);
    ASSERT_EQ(2, a[1]);
    ASSERT_EQ(3, a[2]);
    ASSERT_EQ(4, a[3]);
    ASSERT_EQ(5, a[4]);
    ASSERT_EQ(6, a[5]);

    COUNT_HANDLES(a, N);

    a.sort([](const auto &t1, const auto &t2) { return t1.value > t2.value; });

    ASSERT_EQ(6, a[0]);
    ASSERT_EQ(5, a[1]);
    ASSERT_EQ(4, a[2]);
    ASSERT_EQ(3, a[3]);
    ASSERT_EQ(2, a[4]);
    ASSERT_EQ(1, a[5]);

    COUNT_HANDLES(a, N);

    a.sort();

    ASSERT_EQ(1, a[0]);
    ASSERT_EQ(2, a[1]);
    ASSERT_EQ(3, a[2]);
    ASSERT_EQ(4, a[3]);
    ASSERT_EQ(5, a[4]);
    ASSERT_EQ(6, a[5]);

    COUNT_HANDLES(a, N);
  }

  {
    auto a = arr;
    a.remove({1});
    a.remove({3});
    a.remove({5});

    a.sort();

    ASSERT_EQ(2, a[0]);
    ASSERT_EQ(4, a[1]);
    ASSERT_EQ(6, a[2]);

    COUNT_HANDLES(a, 3);

    a.sort([](const auto &t1, const auto &t2) { return t1.value > t2.value; });

    ASSERT_EQ(6, a[0]);
    ASSERT_EQ(4, a[1]);
    ASSERT_EQ(2, a[2]);
  }
  {
    auto a = arr;
    a.remove({2});
    a.remove({4});
    a.remove({6});

    a.sort();

    ASSERT_EQ(1, a[0]);
    ASSERT_EQ(3, a[1]);
    ASSERT_EQ(5, a[2]);

    COUNT_HANDLES(a, 3);

    a.sort([](const auto &t1, const auto &t2) { return t1.value > t2.value; });

    ASSERT_EQ(5, a[0]);
    ASSERT_EQ(3, a[1]);
    ASSERT_EQ(1, a[2]);
  }
}

#undef COUNT_HANDLES
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
